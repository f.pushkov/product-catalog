import React, { Component } from "react";
import DatePicker from "react-datepicker";
import Select from "react-select";
import ProductCard from "../product-card/ProductCard";

import "react-datepicker/dist/react-datepicker.css";

const $ = window.$;

export default class UpdateProductModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      product: this.props.product,
      nameError: false,
      descriptionError: false,
      imageError: false,
      priceError: false
    };

    this.onChange = this.onChange.bind(this);
    this.onChangeDate = this.onChangeDate.bind(this);
    this.onChangeCategory = this.onChangeCategory.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
  }

  onUpdate(event) {
    event.preventDefault();
    this.props.updateProduct(this.props.product, this.state.product);
    $("#updateProductModal").modal("hide");
  }

  onChange(event) {
    let product = this.state.product;
    product["product" + event.target.name] = event.target.value;
    let nameError = false;
    let priceError = false;
    let descriptionError = false;
    let imageError = false;
    switch (event.target.name) {
      case "Image":
        if (event.target.value.length <= 0) {
          imageError = true;
        }
        break;
      case "Name":
        if (event.target.value.length < 5 || event.target.value.length > 40) {
          nameError = true;
        }
        break;
      case "Description":
        if (event.target.value.length <= 0) {
          descriptionError = true;
        }
        break;
      case "Price":
        if (isNaN(event.target.value) || event.target.value <= 0) {
          priceError = true;
        }
        break;
      default:
    }
    this.setState({
      product,
      imageError,
      nameError,
      descriptionError,
      priceError
    });
  }

  onChangeDate(date) {
    let product = this.state.product;
    product.productDateOfExpiry = date.toISOString();
    this.setState({ product });
  }

  onChangeCategory(category) {
    let product = this.state.product;
    product.productCategory = category.value;
    this.setState({ product });
  }

  static getDerivedStateFromProps(props, state) {
    if (props.product !== state.product) {
      return {
        product: props.product
      };
    }
    return null;
  }

  render() {
    const options = this.props.categoryList.map(category => ({
      value: category,
      label: category
    }));
    const selected = {
      value: this.state.product.productCategory,
      label: this.state.product.productCategory
    };
    const productCard =
      this.props.product !== null ? (
        <ProductCard
          productImage={this.state.product.productImage}
          productName={this.state.product.productName}
          productCategory={this.state.product.productCategory}
          productPrice={this.state.product.productPrice}
          productDateOfExpiry={this.state.product.productDateOfExpiry}
          productDescription={this.state.product.productDescription}
        />
      ) : (
        <div />
      );
    return (
      <div
        className="modal fade"
        id="updateProductModal"
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="updateProductModalLabel">
                Update Product
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-6">
                  {" "}
                  <form onSubmit={this.onUpdate}>
                    <div className="form-group">
                      <label htmlFor="inputProductImageUpdate">
                        Product image URL
                      </label>
                      <input
                        name="Image"
                        type="text"
                        className={
                          this.state.imageError
                            ? "form-control border-danger"
                            : "form-control"
                        }
                        id="inputProductImageUpdate"
                        placeholder="Product image URL"
                        value={this.state.product.productImage}
                        onChange={this.onChange}
                      />
                      <small
                        id="productImageWarningUpdate"
                        className="form-text text-danger"
                        style={{
                          display: this.state.imageError ? "" : "none"
                        }}
                      >
                        Product image length must be greater than zero
                      </small>
                    </div>
                    <div className="form-group">
                      <label htmlFor="inputProductNameUpdate">
                        Product name
                      </label>
                      <input
                        name="Name"
                        type="text"
                        className={
                          this.state.nameError
                            ? "form-control border-danger"
                            : "form-control"
                        }
                        id="inputProductNameUpdate"
                        placeholder="Product name"
                        value={this.state.product.productName}
                        onChange={this.onChange}
                      />
                      <small
                        id="productNameWarningUpdate"
                        className="form-text text-danger"
                        style={{ display: this.state.nameError ? "" : "none" }}
                      >
                        Product name length must be in range 5-40 characters
                      </small>
                    </div>
                    <div className="form-group">
                      <label htmlFor="inputProductDescriptionUpdate">
                        Product description
                      </label>
                      <textarea
                        name="Description"
                        className={
                          this.state.descriptionError
                            ? "form-control border-danger"
                            : "form-control"
                        }
                        id="inputProductDescriptionUpdate"
                        placeholder="Product description"
                        value={this.state.product.productDescription}
                        onChange={this.onChange}
                      />
                      <small
                        id="productDescriptionWarningUpdate"
                        className="form-text text-danger"
                        style={{
                          display: this.state.descriptionError ? "" : "none"
                        }}
                      >
                        Product description length must be greater than zero
                      </small>
                    </div>
                    <div className="form-group">
                      <label htmlFor="inputProductPriceUpdate">
                        Product price
                      </label>
                      <input
                        name="Price"
                        type="text"
                        className={
                          this.state.priceError
                            ? "form-control border-danger"
                            : "form-control"
                        }
                        id="inputProductPriceUpdate"
                        placeholder="Product price"
                        value={this.state.product.productPrice}
                        onChange={this.onChange}
                      />
                      <small
                        id="productPriceWarningUpdate"
                        className="form-text text-danger"
                        style={{ display: this.state.priceError ? "" : "none" }}
                      >
                        Product price must be number and greater than zero
                      </small>
                    </div>
                    <div className="form-group">
                      <label htmlFor="inputProductDateOfExpiryUpdate">
                        Product date of expiry
                      </label>
                      <div id="inputProductDateOfExpiryUpdate">
                        <DatePicker
                          className="form-control"
                          minDate={new Date()}
                          selected={
                            new Date(this.state.product.productDateOfExpiry)
                          }
                          onChange={this.onChangeDate}
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor="inputProductCategoryUpdate">
                        Product category
                      </label>
                      <div id="inputProductCategoryUpdate">
                        <Select
                          value={selected}
                          onChange={this.onChangeCategory}
                          options={options}
                        />
                      </div>
                    </div>
                    <button
                      type="submit"
                      className="btn btn-primary"
                      disabled={
                        this.state.imageError ||
                        this.state.nameError ||
                        this.state.descriptionError ||
                        this.state.priceError
                      }
                    >
                      Submit
                    </button>
                  </form>
                </div>
                <div className="col-lg-6">
                  <div>{productCard}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Navigation extends Component {
  render() {
    let button =
      this.props.isLogin === false ? (
        <button
          type="button"
          className="btn btn-outline-primary my-2 my-sm-0"
          data-toggle="modal"
          data-target="#loginModal"
        >
          Login
        </button>
      ) : (
        <button
          type="button"
          className="btn btn-outline-primary my-2 my-sm-0"
          onClick={this.props.toggleLogin}
        >
          Logout
        </button>
      );

    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container">
            <span className="navbar-brand">Product Catalog</span>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon" />
            </button>

            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link to="/" className="nav-link">
                    Catalog
                  </Link>
                </li>
                <li className="nav-item">
                  <Link
                    to="/admin"
                    className={
                      this.props.isLogin === false
                        ? "nav-link disabled"
                        : "nav-link"
                    }
                  >
                    Admin Panel
                  </Link>
                </li>
              </ul>
              {button}
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

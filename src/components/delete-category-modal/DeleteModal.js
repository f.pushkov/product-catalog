import React, { Component } from "react";
import Select from "react-select";

export default class DeleteModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedOption: null
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(selectedOption) {
    this.setState({ selectedOption });
  }

  render() {
    let replaceCategoryList = this.props.categoryList.slice();
    const index = replaceCategoryList.indexOf(this.props.deleteCategory);
    if (index > -1) replaceCategoryList.splice(index, 1);
    const replaceCategoryOptions = replaceCategoryList.map(category => ({
      value: category,
      label: category
    }));
    return (
      <div className="modal" id="myModal" tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                Delete category: {this.props.deleteCategory}
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="form-group">
                <label htmlFor="replaceCategorySelect">
                  Select category to replace{" "}
                  <span className="badge badge-danger">
                    {this.props.deleteCategory}
                  </span>
                </label>
                <Select
                  id="replaceCategorySelect"
                  options={replaceCategoryOptions}
                  value={this.state.selectedOption}
                  onChange={this.onChange}
                />
              </div>
              <button
                type="button"
                className="btn btn-primary text-center"
                onClick={() => {
                  this.props.replaceDeletedCategory(
                    this.props.deleteCategory,
                    this.state.selectedOption.value
                  );
                }}
                disabled={!this.state.selectedOption}
                data-dismiss="modal"
              >
                Submit
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

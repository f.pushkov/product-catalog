import React, { Component } from "react";

export default class CrudCategoryRow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isEdit: false,
      newCategory: props.category,
      currentCategory: props.category,
      categoryInputClass: "form-control",
      disabled: false
    };

    this.onUpdate = this.onUpdate.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  onSave(event) {
    this.props.updateCategory(
      this.state.currentCategory,
      this.state.newCategory
    );
    this.setState(prevState => ({
      isEdit: false,
      currentCategory: prevState.newCategory
    }));
  }

  onChange(event) {
    this.setState({
      newCategory: event.target.value,
      disabled: event.target.value === "" ? true : false
    });
  }

  onBlur(event) {
    this.setState({
      categoryInputClass:
        this.state.currentCategory === ""
          ? "form-control border-danger"
          : "form-control"
    });
  }

  onUpdate() {
    this.setState(prevState => ({
      isEdit: true,
      newCategory: prevState.currentCategory
    }));
  }

  render() {
    const crudButtons = (
      <div>
        <button
          type="button"
          className="btn btn-warning"
          onClick={this.onUpdate}
        >
          Update
        </button>{" "}
        <button
          type="button"
          className="btn btn-danger"
          onClick={() =>
            this.props.selectCategoryForDelete(this.state.currentCategory)
          }
        >
          Delete
        </button>
      </div>
    );
    const saveButton = (
      <button
        type="button"
        className="btn btn-success"
        onClick={this.onSave}
        disabled={this.state.disabled}
      >
        Save
      </button>
    );

    return (
      <tr>
        <th scope="row">{this.props.index}</th>
        <td>
          {!this.state.isEdit ? (
            this.state.currentCategory
          ) : (
            <input
              type="text"
              name="category"
              className={this.state.categoryInputClass}
              id="updateCategory"
              placeholder="Enter category name"
              value={this.state.newCategory}
              onBlur={this.onBlur}
              onChange={this.onChange}
            />
          )}
        </td>
        <td>{!this.state.isEdit ? crudButtons : saveButton}</td>
      </tr>
    );
  }
}

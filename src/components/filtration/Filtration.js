import React, { Component } from "react";
import Select from "react-select";

export default class Filtration extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selected: null
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(selected) {
    this.setState({ selected });
  }

  onSubmit(event) {
    event.preventDefault();
    if (this.state.selected === null) {
      this.props.submitCallback(null);
    } else {
      const selected = this.state.selected.map(category => category.value);
      this.props.submitCallback(selected);
    }
  }

  render() {
    const options = this.props.categoryList.map(category => ({
      value: category,
      label: category
    }));

    return (
      <div className="col-lg-3 col-xl-2">
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label
              htmlFor="selectCategories"
              className="d-block pl-2"
              style={{ opacity: this.props.disabled ? "0.5" : "1" }}
            >
              Categories
            </label>
            <Select
              value={this.state.selected}
              onChange={this.onChange}
              options={options}
              isMulti
              isDisabled={this.props.disabled}
            />
          </div>
          <button
            type="submit"
            className="btn btn-primary"
            disabled={this.props.disabled}
          >
            Apply
          </button>
        </form>
      </div>
    );
  }
}

import React, { Component } from "react";
import DatePicker from "react-datepicker";
import Select from "react-select";
import ProductCard from "../product-card/ProductCard";

import "react-datepicker/dist/react-datepicker.css";

const $ = window.$;

export default class CreateProductModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      product: {
        productImage: "",
        productName: "",
        productDescription: "",
        productPrice: "",
        productDateOfExpiry: new Date(),
        productCategory: ""
      },
      selected: null,
      imageError: false,
      nameError: false,
      descriptionError: false,
      categoryError: false,
      priceError: false
    };

    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onChangeDate = this.onChangeDate.bind(this);
    this.onChangeCategory = this.onChangeCategory.bind(this);
    this.onCreate = this.onCreate.bind(this);
  }

  onCreate(event) {
    event.preventDefault();
    this.props.createProduct(this.state.product);
    this.setState({
      product: {
        productImage: "",
        productName: "",
        productDescription: "",
        productPrice: "",
        productDateOfExpiry: new Date(),
        productCategory: ""
      },
      selected: null,
      imageError: false,
      nameError: false,
      descriptionError: false,
      categoryError: false,
      priceError: false
    });
    $("#createProductModal").modal("hide");
  }

  onChange(event) {
    let product = this.state.product;
    product["product" + event.target.name] = event.target.value;
    let nameError = false;
    let priceError = false;
    let descriptionError = false;
    let imageError = false;
    switch (event.target.name) {
      case "Image":
        if (event.target.value.length <= 0) {
          imageError = true;
        }
        break;
      case "Name":
        if (event.target.value.length < 5 || event.target.value.length > 40) {
          nameError = true;
        }
        break;
      case "Description":
        if (event.target.value.length <= 0) {
          descriptionError = true;
        }
        break;
      case "Price":
        if (isNaN(event.target.value) || event.target.value <= 0) {
          priceError = true;
        }
        break;
      default:
    }
    this.setState({
      product,
      imageError,
      nameError,
      descriptionError,
      priceError
    });
  }

  onBlur(event) {
    let nameError = this.state.nameError;
    let priceError = this.state.priceError;
    let descriptionError = this.state.descriptionError;
    let imageError = this.state.imageError;
    switch (event.target.name) {
      case "Image":
        if (event.target.value.length <= 0) {
          imageError = true;
        }
        break;
      case "Name":
        if (event.target.value.length < 5 || event.target.value.length > 40) {
          nameError = true;
        }
        break;
      case "Description":
        if (event.target.value.length <= 0) {
          descriptionError = true;
        }
        break;
      case "Price":
        if (isNaN(event.target.value) || event.target.value <= 0) {
          priceError = true;
        }
        break;
      default:
    }
    this.setState({ nameError, descriptionError, priceError, imageError });
  }

  onChangeDate(date) {
    let product = this.state.product;
    product.productDateOfExpiry = date.toISOString();
    this.setState({ product });
  }

  onChangeCategory(category) {
    let product = this.state.product;
    product.productCategory = category.value;
    let categoryError = false;
    this.setState({ product, categoryError });
  }
  render() {
    const options = this.props.categoryList.map(category => ({
      value: category,
      label: category
    }));
    const productCard = (
      <ProductCard
        productImage={this.state.product.productImage}
        productName={this.state.product.productName}
        productCategory={this.state.product.productCategory}
        productPrice={this.state.product.productPrice}
        productDateOfExpiry={this.state.product.productDateOfExpiry}
        productDescription={this.state.product.productDescription}
      />
    );
    return (
      <div
        className="modal fade"
        id="createProductModal"
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="createProductModalLabel">
                Create Product
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-lg-6">
                  {" "}
                  <form onSubmit={this.onCreate}>
                    <div className="form-group">
                      <label htmlFor="inputProductImage">
                        Product image URL
                      </label>
                      <input
                        name="Image"
                        type="text"
                        className={
                          this.state.imageError
                            ? "form-control border-danger"
                            : "form-control"
                        }
                        id="inputProductImage"
                        placeholder="Product image URL"
                        value={this.state.product.productImage}
                        onChange={this.onChange}
                        onBlur={this.onBlur}
                      />
                      <small
                        id="productImageWarningUpdate"
                        className="form-text text-danger"
                        style={{
                          display: this.state.imageError ? "" : "none"
                        }}
                      >
                        Product image length must be greater than zero
                      </small>
                    </div>
                    <div className="form-group">
                      <label htmlFor="inputProductName">Product name</label>
                      <input
                        name="Name"
                        type="text"
                        className={
                          this.state.nameError
                            ? "form-control border-danger"
                            : "form-control"
                        }
                        id="inputProductName"
                        placeholder="Product name"
                        value={this.state.product.productName}
                        onChange={this.onChange}
                        onBlur={this.onBlur}
                      />
                      <small
                        id="productNameWarning"
                        className="form-text text-danger"
                        style={{ display: this.state.nameError ? "" : "none" }}
                      >
                        Product name length must be in range 5-40 characters
                      </small>
                    </div>
                    <div className="form-group">
                      <label htmlFor="inputProductDescription">
                        Product description
                      </label>
                      <textarea
                        name="Description"
                        className={
                          this.state.descriptionError
                            ? "form-control border-danger"
                            : "form-control"
                        }
                        id="inputProductDescription"
                        placeholder="Product description"
                        value={this.state.product.productDescription}
                        onChange={this.onChange}
                        onBlur={this.onBlur}
                      />
                      <small
                        id="productDescriptionWarning"
                        className="form-text text-danger"
                        style={{
                          display: this.state.descriptionError ? "" : "none"
                        }}
                      >
                        Product description length must be greater than zero
                      </small>
                    </div>
                    <div className="form-group">
                      <label htmlFor="inputProductPrice">Product price</label>
                      <input
                        name="Price"
                        type="text"
                        className={
                          this.state.priceError
                            ? "form-control border-danger"
                            : "form-control"
                        }
                        id="inputProductPrice"
                        placeholder="Product price"
                        value={this.state.product.productPrice}
                        onChange={this.onChange}
                        onBlur={this.onBlur}
                      />
                      <small
                        id="productPriceWarning"
                        className="form-text text-danger"
                        style={{ display: this.state.priceError ? "" : "none" }}
                      >
                        Product price must be number and greater than zero
                      </small>
                    </div>
                    <div className="form-group">
                      <label htmlFor="inputProductDateOfExpiry">
                        Product date of expiry
                      </label>
                      <div id="inputProductDateOfExpiry">
                        <DatePicker
                          className="form-control"
                          minDate={new Date()}
                          selected={
                            new Date(this.state.product.productDateOfExpiry)
                          }
                          onChange={this.onChangeDate}
                        />
                      </div>
                    </div>
                    <div className="form-group">
                      <label htmlFor="inputProductCategory">
                        Product category
                      </label>
                      <div id="inputProductCategory">
                        <Select
                          value={this.state.selected}
                          onChange={this.onChangeCategory}
                          options={options}
                        />
                      </div>
                    </div>
                    <button
                      type="submit"
                      className="btn btn-primary"
                      disabled={
                        this.state.imageError ||
                        this.state.nameError ||
                        this.state.descriptionError ||
                        this.state.categoryError ||
                        this.state.priceError ||
                        this.state.product.productImage === "" ||
                        this.state.product.productName === "" ||
                        this.state.product.productDescription === "" ||
                        this.state.product.productPrice === "" ||
                        this.state.productCategory === ""
                      }
                    >
                      Submit
                    </button>
                  </form>
                </div>
                <div className="col-lg-6">
                  <div>{productCard}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

import React, { Component } from "react";
import { Route, BrowserRouter } from "react-router-dom";
import Navigation from "./navigation/Navigation";
import LoginModal from "./login-modal/LoginModal";
import Catalog from "./catalog/Catalog";
import AdminPanel from "./admin-panel/AdminPanel";
import axios from "axios";
import faker from "faker";

export default class App extends Component {
  constructor() {
    super();
    //hardcoded product list
    let productList = JSON.parse(localStorage.getItem("productList"));
    let categoryList = JSON.parse(localStorage.getItem("categoryList"));
    const items = 72;
    const itemsPerPage = 12;
    let isLoaded = false;
    if (productList === null || categoryList === null) {
      productList = [];
      categoryList = [];
      isLoaded = true;
      for (let i = 0; i < items; i++) {
        productList.push({
          productImage: "",
          productName: faker.commerce.productName(),
          productDescription: faker.lorem.sentence(13),
          productPrice: faker.commerce.price(),
          productDateOfExpiry: faker.date.future(),
          productCategory: faker.commerce.department(),
          number: i
        });
      }
      productList
        .map(product => product.productCategory)
        .forEach(category => {
          if (!categoryList.includes(category)) categoryList.push(category);
        });
    }

    this.state = {
      isLogin: false,
      isLoaded: isLoaded,
      productList: productList,
      selectedProductList: [],
      categoryList: categoryList.sort(),
      selectedCategory: [],
      items: items,
      selectedItems: 0,
      itemsPerPage: itemsPerPage
    };

    localStorage.setItem("productList", JSON.stringify(this.state.productList));
    localStorage.setItem(
      "categoryList",
      JSON.stringify(this.state.categoryList)
    );

    this.setSelectedCategory = this.setSelectedCategory.bind(this);
    this.getPics = this.getPics.bind(this);
    this.toggleLogin = this.toggleLogin.bind(this);
    this.updateCategory = this.updateCategory.bind(this);
    this.replaceDeletedCategory = this.replaceDeletedCategory.bind(this);
    this.createCategory = this.createCategory.bind(this);
    this.updateProduct = this.updateProduct.bind(this);
    this.createProduct = this.createProduct.bind(this);
    this.deleteProduct = this.deleteProduct.bind(this);
  }

  deleteProduct(product) {
    let productList = this.state.productList.slice();
    let index = productList.indexOf(product);
    productList.splice(index, 1);
    this.setState({
      productList,
      selectedCategory: [],
      selectedProductList: [],
      selectedItems: 0
    });
    localStorage.setItem("productList", JSON.stringify(productList));
  }

  createProduct(newProduct) {
    newProduct.number = this.state.productList.length;
    let newProductList = this.state.productList.slice();
    newProductList.push(newProduct);
    this.setState({
      productList: newProductList,
      selectedCategory: [],
      selectedProductList: [],
      selectedItems: 0
    });
    localStorage.setItem("productList", JSON.stringify(newProductList));
  }

  updateProduct(oldProduct, newProduct) {
    let productList = this.state.productList.slice();
    const index = productList.indexOf(oldProduct);
    productList.splice(index, 1, newProduct);
    this.setState({ productList });
    localStorage.setItem("productList", JSON.stringify(productList));
  }

  createCategory(category) {
    let categoryList = this.state.categoryList.slice();
    categoryList.push(category);
    this.setState({
      categoryList: categoryList.sort(),
      selectedCategory: [],
      selectedProductList: [],
      selectedItems: 0
    });
    localStorage.setItem("categoryList", JSON.stringify(categoryList));
  }

  replaceDeletedCategory(deleteCategory, replaceCategory) {
    let updatedCategoryList = this.state.categoryList.slice();
    const index = updatedCategoryList.indexOf(deleteCategory);
    updatedCategoryList.splice(index, 1);
    let updatedProductList = this.state.productList.slice();
    updatedProductList.forEach((product, index, array) => {
      if (product.productCategory === deleteCategory)
        array[index].productCategory = replaceCategory;
    });
    this.setState({
      productList: updatedProductList,
      categoryList: updatedCategoryList,
      selectedCategory: [],
      selectedProductList: [],
      selectedItems: 0
    });
    localStorage.setItem("productList", JSON.stringify(updatedProductList));
    localStorage.setItem("categoryList", JSON.stringify(updatedCategoryList));
  }

  updateCategory(oldCategory, newCategory) {
    let categoryListUpdated = this.state.categoryList.slice();
    let productListUpdated = this.state.productList.slice();
    const index = categoryListUpdated.indexOf(oldCategory);
    categoryListUpdated[index] = newCategory;
    productListUpdated.forEach((product, index, array) => {
      if (product.productCategory === oldCategory)
        array[index].productCategory = newCategory;
    });
    this.setState({
      categoryList: categoryListUpdated,
      productList: productListUpdated,
      selectedProductList: [],
      selectedCategory: [],
      selectedItems: 0
    });
    localStorage.setItem("categoryList", JSON.stringify(categoryListUpdated));
    localStorage.setItem("productList", JSON.stringify(productListUpdated));
  }

  toggleLogin() {
    this.setState(prevState => ({ isLogin: !prevState.isLogin }));
  }

  setSelectedCategory(selected) {
    if (selected === null) {
      this.setState({
        selectedCategory: [],
        selectedItems: 0,
        selectedProductList: []
      });
    } else {
      const selectedProducts = [];
      let products = this.state.productList;

      products.forEach(product => {
        if (selected.includes(product.productCategory))
          selectedProducts.push(product);
      });
      this.setState({
        selectedCategory: selected,
        selectedItems: selectedProducts.length,
        selectedProductList: selectedProducts
      });
    }
  }

  async getPics() {
    if (this.state.isLoaded) {
      for (let i = 0; i < this.state.items; i++) {
        axios.get("https://picsum.photos/500/300").then(response => {
          let productList = this.state.productList.slice();
          productList[i].productImage = response.request.responseURL;
          this.setState({ productList: productList });
          localStorage.setItem("productList", JSON.stringify(productList));
        });
      }
    }
  }

  componentDidMount() {
    this.getPics();
  }

  render() {
    return (
      <BrowserRouter>
        <LoginModal toggleLogin={this.toggleLogin} />
        <Navigation
          isLogin={this.state.isLogin}
          toggleLogin={this.toggleLogin}
        />
        <Route
          exact
          path="/"
          render={props => (
            <Catalog
              {...props}
              categoryList={this.state.categoryList}
              submitCallback={this.setSelectedCategory}
              disabled={!this.state.isLogin}
              productList={this.state.selectedProductList}
              selectedCategory={this.state.selectedCategory}
              items={this.state.selectedItems}
              itemsPerPage={this.state.itemsPerPage}
              isLogin={this.state.isLogin}
            />
          )}
        />
        <Route
          path="/admin"
          render={props => (
            <AdminPanel
              {...props}
              categoryList={this.state.categoryList}
              productList={this.state.productList}
              updateCategory={this.updateCategory}
              createCategory={this.createCategory}
              updateProduct={this.updateProduct}
              createProduct={this.createProduct}
              deleteProduct={this.deleteProduct}
              isLogin={this.state.isLogin}
              replaceDeletedCategory={this.replaceDeletedCategory}
            />
          )}
        />
      </BrowserRouter>
    );
  }
}

import React, { Component } from "react";

const $ = window.$;

export default class LoginModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: "",
      password: "",
      loginDanger: "none",
      loginInputClass: "form-control",
      passwordDanger: "none",
      passwordInputClass: "form-control",
      submitDanger: "none",
      disabledSubmit: true
    };

    this.onChange = this.onChange.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();
    if (this.state.login === "admin" && this.state.password === "admin") {
      this.setState({ login: "", password: "" });
      this.props.toggleLogin();
      $("#loginModal").modal("toggle");
    } else {
      this.setState({ submitDanger: "" });
      setInterval(() => this.setState({ submitDanger: "none" }), 3000);
    }
  }

  onChange(event) {
    let obj = { login: this.state.login, password: this.state.password };
    obj[event.target.name] = event.target.value;
    this.setState({
      [event.target.name]: event.target.value,
      disabledSubmit: obj.login === "" || obj.password === "" ? true : false
    });
  }

  onBlur(event) {
    this.setState({
      [event.target.name + "Danger"]:
        this.state[event.target.name] === "" ? "" : "none",
      [event.target.name + "InputClass"]:
        this.state[event.target.name] === ""
          ? "form-control border-danger"
          : "form-control"
    });
  }

  render() {
    return (
      <div
        className="modal fade"
        id="loginModal"
        tabIndex="-1"
        role="dialog"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="loginModalLabel">
                Please Login to Continue
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <div
                className="alert alert-danger"
                role="alert"
                style={{ display: this.state.submitDanger }}
              >
                Wrong login or password. Try "admin"/"admin".
              </div>
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <label htmlFor="inputLogin">Login</label>
                  <input
                    type="text"
                    name="login"
                    className={this.state.loginInputClass}
                    id="inputLogin"
                    placeholder="Enter login"
                    value={this.state.login}
                    onChange={this.onChange}
                    onBlur={this.onBlur}
                  />
                  <small
                    id="loginHelp"
                    className="form-text text-danger"
                    style={{ display: this.state.loginDanger }}
                  >
                    Login is required
                  </small>
                </div>
                <div className="form-group">
                  <label htmlFor="inputPassword">Password</label>
                  <input
                    type="password"
                    name="password"
                    className={this.state.passwordInputClass}
                    id="inputPassword"
                    placeholder="Enter password"
                    value={this.state.password}
                    onChange={this.onChange}
                    onBlur={this.onBlur}
                  />
                  <small
                    id="emailHelp"
                    className="form-text text-danger"
                    style={{ display: this.state.passwordDanger }}
                  >
                    Password is required
                  </small>
                </div>
                <button
                  type="submit"
                  className="btn btn-primary"
                  disabled={this.state.disabledSubmit}
                >
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

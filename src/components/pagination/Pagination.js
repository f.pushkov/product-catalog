import React, { Component } from "react";

export default class Pagination extends Component {
  render() {
    const pageNumbers = [];
    for (
      let i = 1;
      i <= Math.ceil(this.props.items / this.props.itemsPerPage);
      i++
    ) {
      pageNumbers.push(i);
    }

    const pages = pageNumbers.map(number => (
      <li
        key={number}
        className={
          this.props.currentPage === number ? "page-item active" : "page-item"
        }
      >
        <a
          href="#"
          className="page-link"
          onClick={() => this.props.changePage(number)}
        >
          {number}
        </a>
      </li>
    ));

    return (
      <nav
        aria-label="Product Catalog Pagination"
        className="d-flex justify-content-center"
      >
        <ul className="pagination">{pages}</ul>
      </nav>
    );
  }
}

import React, { Component } from "react";

export default class CrudProductRow extends Component {
  render() {
    return (
      <tr>
        <th scope="row">{this.props.index}</th>
        <td>{this.props.product.productName}</td>
        <td>{this.props.product.productPrice}</td>
        <td>
          {new Date(this.props.product.productDateOfExpiry).toUTCString()}
        </td>
        <td>{this.props.product.productCategory}</td>
        <td>
          <button
            type="button"
            className="btn btn-info"
            data-toggle="modal"
            data-target="#infoProductModal"
            onClick={() => this.props.showProductInfoModal(this.props.product)}
          >
            Info
          </button>{" "}
          <button
            type="button"
            className="btn btn-warning"
            data-toggle="modal"
            data-target="#updateProductModal"
            onClick={() =>
              this.props.showUpdateProductModal(this.props.product)
            }
          >
            Update
          </button>{" "}
          <button
            type="button"
            className="btn btn-danger"
            onClick={() => this.props.deleteProduct(this.props.product)}
          >
            Delete
          </button>
        </td>
      </tr>
    );
  }
}

import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import CrudCategoriesPanel from "../crud-categories-panel/CrudCategoriesPanel";
import CrudProductsPanel from "../crud-products-panel/CrudProductsPanel";

export default class AdminPanel extends Component {
  render() {
    return (
      <div className="container-fluid">
        {this.props.isLogin ? (
          <div>
            <div className="container d-flex">
              <h3 className="font-weight-light">CRUD Operations</h3>
              <Link
                to="/admin/categories"
                className="btn btn-dark ml-5 mr-3"
                role="button"
              >
                Categories
              </Link>
              <Link to="/admin/products" className="btn btn-dark" role="button">
                Products
              </Link>
            </div>

            <Route
              path="/admin/categories"
              render={props => (
                <CrudCategoriesPanel
                  categoryList={this.props.categoryList}
                  updateCategory={this.props.updateCategory}
                  createCategory={this.props.createCategory}
                  isLogin={this.props.isLogin}
                  replaceDeletedCategory={this.props.replaceDeletedCategory}
                />
              )}
            />
            <Route
              path="/admin/products"
              render={props => (
                <CrudProductsPanel
                  updateProduct={this.props.updateProduct}
                  createProduct={this.props.createProduct}
                  deleteProduct={this.props.deleteProduct}
                  productList={this.props.productList}
                  categoryList={this.props.categoryList}
                />
              )}
            />
          </div>
        ) : (
          <h1 className="text-center font-weight-light mt-5">
            Please, login to continue
          </h1>
        )}
      </div>
    );
  }
}

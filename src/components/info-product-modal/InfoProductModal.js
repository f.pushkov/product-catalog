import React, { Component } from "react";
import ProductCard from "../product-card/ProductCard";

export default class InfoProductModal extends Component {
  render() {
    const productCard =
      this.props.product !== null ? (
        <ProductCard
          productImage={this.props.product.productImage}
          productName={this.props.product.productName}
          productCategory={this.props.product.productCategory}
          productPrice={this.props.product.productPrice}
          productDateOfExpiry={this.props.product.productDateOfExpiry}
          productDescription={this.props.product.productDescription}
        />
      ) : (
        <div />
      );
    return (
      <div
        className="modal fade"
        id="infoProductModal"
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="infoProductModalLabel">
                Product Info
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">{productCard}</div>
          </div>
        </div>
      </div>
    );
  }
}

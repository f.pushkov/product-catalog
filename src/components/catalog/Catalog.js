import React, { Component } from "react";
import Filtration from "../filtration/Filtration";
import ProductGrid from "../product-grid/ProductGrid";

export default class Catalog extends Component {
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <Filtration
            categoryList={this.props.categoryList}
            submitCallback={this.props.submitCallback}
            disabled={this.props.disabled}
          />
          <ProductGrid
            productList={this.props.productList}
            selectedCategory={this.props.selectedCategory}
            items={this.props.items}
            itemsPerPage={this.props.itemsPerPage}
            isLogin={this.props.isLogin}
          />
        </div>
      </div>
    );
  }
}

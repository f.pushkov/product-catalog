import React, { Component } from "react";
import CrudProductRow from "../crud-product-row/CrudProductRow";
import InfoProductModal from "../info-product-modal/InfoProductModal";
import UpdateProductModal from "../update-product-modal/UpdateProductModal";
import CreateProductModal from "../create-product-modal/CreateProductModal";

export default class CrudProductsPanel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedProduct: props.productList[0]
    };

    this.showProductInfoModal = this.showProductInfoModal.bind(this);
    this.showUpdateProductModal = this.showUpdateProductModal.bind(this);
  }

  showProductInfoModal(product) {
    this.setState({ selectedProduct: product });
  }

  showUpdateProductModal(product) {
    this.setState({ selectedProduct: product });
  }

  render() {
    const products = this.props.productList.map((product, index) => (
      <CrudProductRow
        key={product.number}
        product={product}
        index={index + 1}
        showProductInfoModal={this.showProductInfoModal}
        showUpdateProductModal={this.showUpdateProductModal}
        deleteProduct={this.props.deleteProduct}
      />
    ));
    return (
      <div className="container-fluid">
        <InfoProductModal product={this.state.selectedProduct} />
        <UpdateProductModal
          product={this.state.selectedProduct}
          categoryList={this.props.categoryList}
          updateProduct={this.props.updateProduct}
        />
        <CreateProductModal
          categoryList={this.props.categoryList}
          createProduct={this.props.createProduct}
        />
        <button
          type="button"
          className="btn btn-primary my-2"
          data-toggle="modal"
          data-target="#createProductModal"
        >
          Add Product
        </button>
        <table className="table table-hover">
          <thead className="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Price</th>
              <th scope="col">Date of Expiry</th>
              <th scope="col">Category</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>{products}</tbody>
        </table>
      </div>
    );
  }
}

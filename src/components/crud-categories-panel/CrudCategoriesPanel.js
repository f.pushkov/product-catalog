import React, { Component } from "react";
import CrudCategoryRow from "../crud-category-row/CrudCategoryRow";
import DeleteModal from "../delete-category-modal/DeleteModal";

const $ = window.$;

export default class CrudCategoriesPanel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      createCategory: "",
      deleteCategory: "",
      isCreate: false,
      notificationCategoryExists: "none"
    };

    this.selectCategoryForDelete = this.selectCategoryForDelete.bind(this);
    this.togleCreate = this.togleCreate.bind(this);
    this.onCreateChange = this.onCreateChange.bind(this);
    this.onCreate = this.onCreate.bind(this);
  }

  onCreate(event) {
    event.preventDefault();
    const index = this.props.categoryList.indexOf(this.state.createCategory);
    if (index === -1) {
      this.props.createCategory(this.state.createCategory);
      this.setState({ createCategory: "", isCreate: false });
    } else {
      this.setState({ notificationCategoryExists: "" });
      setTimeout(
        () => this.setState({ notificationCategoryExists: "none" }),
        3000
      );
    }
  }

  onCreateChange(event) {
    this.setState({ createCategory: event.target.value });
  }

  togleCreate() {
    this.setState(prevState => ({ isCreate: !prevState.isCreate }));
  }

  selectCategoryForDelete(category) {
    this.setState({ deleteCategory: category });
    $("#myModal").modal("show");
  }

  render() {
    let rows = this.props.categoryList
      .sort()
      .map((category, index) => (
        <CrudCategoryRow
          key={category}
          index={index + 1}
          category={category}
          updateCategory={this.props.updateCategory}
          selectCategoryForDelete={this.selectCategoryForDelete}
        />
      ));
    return (
      <div className="container-fluid">
        <DeleteModal
          deleteCategory={this.state.deleteCategory}
          categoryList={this.props.categoryList}
          replaceDeletedCategory={this.props.replaceDeletedCategory}
        />
        {this.props.isLogin ? (
          <div>
            {!this.state.isCreate ? (
              <button
                type="button"
                className="btn btn-primary my-2"
                onClick={this.togleCreate}
              >
                Add Category
              </button>
            ) : (
              <div>
                <div
                  className="alert alert-danger"
                  role="alert"
                  style={{ display: this.state.notificationCategoryExists }}
                >
                  Such category already exists!
                </div>
                <form className="form-inline pb-2">
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control mr-2"
                      id="createCategoryInput"
                      placeholder="Enter new category"
                      value={this.state.createCategory}
                      onChange={this.onCreateChange}
                    />
                  </div>
                  <button
                    type="submit"
                    className="btn btn-success"
                    disabled={!this.state.createCategory}
                    onClick={this.onCreate}
                  >
                    Create
                  </button>
                </form>
              </div>
            )}
            <table className="table table-hover">
              <thead className="thead-dark">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Category</th>
                  <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>{rows}</tbody>
            </table>
          </div>
        ) : (
          <h2 className="text-muted mt-5 font-weight-bold">
            Please, login to continue
          </h2>
        )}
      </div>
    );
  }
}

import React, { Component } from "react";
import ProductCard from "../product-card/ProductCard";
import Pagination from "../pagination/Pagination";

export default class ProductGrid extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: 1
    };

    this.changePage = this.changePage.bind(this);
    this.getProductsOnCurrentPage = this.getProductsOnCurrentPage.bind(this);
  }

  changePage(number) {
    this.setState({ currentPage: number });
  }

  getProductsOnCurrentPage() {
    return this.props.productList.filter(
      (product, index) =>
        index < this.state.currentPage * this.props.itemsPerPage &&
        index >=
          this.state.currentPage * this.props.itemsPerPage -
            this.props.itemsPerPage
    );
  }

  render() {
    const productsWrappedInProductCard = this.getProductsOnCurrentPage().map(
      product => (
        <div className="col-md-4 col-xl-3 my-3" key={product.number}>
          <ProductCard
            productImage={product.productImage}
            productName={product.productName}
            productCategory={product.productCategory}
            productPrice={product.productPrice}
            productDateOfExpiry={product.productDateOfExpiry}
            productDescription={product.productDescription}
          />
        </div>
      )
    );

    return (
      <div className="col-lg-9 col-xl-10">
        {this.props.isLogin ? (
          <div>
            <div className="mx-4">
              <div className="row my-5">{productsWrappedInProductCard}</div>
            </div>
            <Pagination
              items={this.props.items}
              itemsPerPage={this.props.itemsPerPage}
              currentPage={this.state.currentPage}
              changePage={this.changePage}
            />
          </div>
        ) : (
          <div />
        )}
      </div>
    );
  }
}

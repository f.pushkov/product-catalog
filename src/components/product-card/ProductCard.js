import React, { Component } from "react";

export default class ProductCard extends Component {
  render() {
    return (
      <div className="card h-100">
        <img
          className="card-img-top"
          src={this.props.productImage}
          alt="Product"
        />
        <div className="card-body pb-5">
          <h5 className="card-title text-center mb-2">
            {this.props.productName}
          </h5>
          <p className="card-text mb-2" style={{ fontSize: "15px" }}>
            {this.props.productDescription}
          </p>
          <hr className="my-2" />
          <p className="card-text mb-2 " style={{ fontSize: "15px" }}>
            Price:{" "}
            <span className="badge badge-warning">
              {this.props.productPrice + " $"}
            </span>
          </p>
          <hr className="my-2" />
          <p className="card-text mb-2 " style={{ fontSize: "15px" }}>
            Date of Expiry:{" "}
            <span className="badge badge-danger">
              {new Date(this.props.productDateOfExpiry).toLocaleDateString()}
            </span>
          </p>
        </div>
        <p
          className="text-right mb-0"
          style={{ position: "absolute", bottom: "10px", right: "10px" }}
        >
          <span className="badge badge-primary p-2">
            {this.props.productCategory}
          </span>
        </p>
      </div>
    );
  }
}
